FROM python:3.8

RUN apt update \
 && apt install ca-certificates tzdata ffmpeg -y \
 && apt autoremove --purge --auto-remove -y && apt clean && rm -rf /var/lib/apt/lists/*

COPY src/req.txt /usr/src/req.txt
RUN python -m pip install --upgrade pip
RUN cd /usr/src && pip3 install https://github.com/alphacep/vosk-api/releases/download/v0.3.42/vosk-0.3.42-py3-none-linux_aarch64.whl

RUN cd /usr/src \
 && pip3 install --no-cache-dir -r /usr/src/req.txt \
 && cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime

COPY src/vosk-api /app

WORKDIR /app/python/example/

# Use the ping endpoint as a healthcheck,
# so Docker knows if the API is still running ok or needs to be restarted
HEALTHCHECK --interval=21s --timeout=3s --start-period=10s CMD curl --fail http://localhost:8080/ping || exit 1

EXPOSE 8080

CMD ["uvicorn", "api_dir.api:app", "--host", "0.0.0.0", "--port", "8080"]
