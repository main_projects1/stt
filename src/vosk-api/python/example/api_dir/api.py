from vosk import Model, KaldiRecognizer, SetLogLevel
import subprocess
from fastapi import FastAPI, File, UploadFile, Query
from pydantic import BaseModel
from enum import Enum
from typing import List

app = FastAPI()

import sys

sys.path.append('../..')
sys.path.append('..')

from tools import recognizer

# load all models
models = {

    "en": recognizer.Recognizer('model_en'),
    "ru": recognizer.Recognizer('model_ru'),
}


class ModelLanguage(str, Enum):
    ru = "ru"
    en = "en"


class UserRequestIn(BaseModel):
    model_language: ModelLanguage
    # model_size: ModelSize = "sm"


class TextOut(BaseModel):
    start: float
    end: float
    conf: float = Query(ge=0, le=1)
    word: str


class EntitiesOut(BaseModel):
    anonymized_text: str
    text_time: List[TextOut]


@app.post("/upload-file/")
async def create_upload_file(uploaded_file: UploadFile = File(...)):
    file_location = f"files/{uploaded_file.filename}"
    with open(file_location, "wb+") as file_object:
        file_object.write(uploaded_file.file.read())
    return {"info": f"file '{uploaded_file.filename}' saved at '{file_location}'"}


@app.post("/api/entities", response_model=EntitiesOut)
def extract_entities(uploaded_file: UploadFile = File(...), model_language: ModelLanguage = 'ru'):

    file_location = f"files/{uploaded_file.filename}"

    with open(file_location, "wb+") as file_object:
        file_object.write(uploaded_file.file.read())
    print(f"file '{uploaded_file.filename}' saved at '{file_location}'")

    # determine language
    lang = model_language

    # make recognition on model language
    anonymized_text, text_time = models[lang].recognize(str(file_location))

    entities = [
        {
            "start": ent['start'],
            "end": ent['end'],
            "conf": ent['conf'],
            "word": ent['word'],
        }
        for res in text_time if res != {'text': ''} for ent in res['result']
    ]

    # Добавляем пунктуацию
    # cased = subprocess.check_output('python3 recasepunc/recasepunc.py predict recasepunc/checkpoint', text=True, input=anonymized_text)
    cased = str()
    cased = subprocess.check_output(['python', 'recasepunc/recasepunc.py', 'predict', 'recasepunc/checkpoint'],
                                    text=True, input=anonymized_text
                                    )

    return {"anonymized_text": str(cased), "text_time": entities}


@app.get('/api')
def hello():
    return {"message": "Hello"}

# if __name__ == '__main__':
#    uvicorn.run(app, host='127.0.0.1', port=8000)
