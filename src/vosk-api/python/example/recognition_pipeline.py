#!/usr/bin/env python3

from vosk import Model, KaldiRecognizer, SetLogLevel
import subprocess
import json
import os
import time
import argparse
import glob
from pathlib import Path
from tqdm import tqdm
from multiprocessing import Pool, cpu_count
from multiprocessing.pool import ThreadPool as Pool

SetLogLevel(0)


class Recognition:

    def __init__(self):

        if not os.path.exists("model_ru"):
            print("Please download the model from https://alphacephei.com/vosk/models and unpack as 'model' in the "
                  "current folder.")
            exit(1)

        self.model = Model("model_ru")

    def process_audio(self, path):

        sample_rate = 16000
        rec = KaldiRecognizer(self.model, sample_rate)
        rec.SetWords(True)

        process = subprocess.Popen(['ffmpeg', '-loglevel', 'quiet', '-i',
                                    path,
                                    '-ar', str(sample_rate), '-ac', '1', '-f', 's16le', '-'],
                                   stdout=subprocess.PIPE)

        os.makedirs(str(Path(OUTPUT_DIR)), exist_ok=True)

        results, textResults = [], []

        while True:
            data = process.stdout.read(4000)
            if len(data) == 0:
                break
            if rec.AcceptWaveform(data):

                resultDict = json.loads(rec.Result())
                results.append(resultDict)
                textResults.append(resultDict.get("text", ""))

            else:
                print(rec.PartialResult())

        # process "final" result
        resultDict = json.loads(rec.FinalResult())
        results.append(resultDict)
        textResults.append(resultDict.get("text", ""))

        # write results to a file
        with open(f'{OUTPUT_DIR}/{Path(path).stem}.json', 'w') as output:
            print(json.dumps(results, indent=4, ensure_ascii=False), file=output)

        # write text portion of results to a file
        with open(f'{OUTPUT_DIR}/{Path(path).stem}_text.json', 'w') as output:
            print(json.dumps(textResults, indent=4, ensure_ascii=False), file=output)

        os.remove(path)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Запуск процедуры распознавания речи')
    parser.add_argument('wav', metavar='WAV', help='Путь к .WAV файлам аудио')
    parser.add_argument('output', metavar='OUT', help='Путь к директории с результатами распознавания')
    args = parser.parse_args()

    WAV_DIR = Path(args.wav)
    OUTPUT_DIR = Path(args.output)

    recognizer = Recognition()

    while True:
        wavs = glob.glob(str(WAV_DIR / '*'))
        print(wavs)
        if wavs:
            print("Обнаружено {} .WAV файлов".format(len(wavs)))

            PROCESSES = cpu_count()
            pool = Pool(PROCESSES)

            print("Количество процессов: {}".format(PROCESSES))

            for _ in tqdm(pool.imap(recognizer.process_audio, wavs), total=len(wavs)):
                pass

            pool.close()
            pool.join()

            break
            time.sleep(300)

        time.sleep(100)
