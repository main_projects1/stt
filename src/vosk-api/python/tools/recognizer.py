from pathlib import Path
from vosk import Model, KaldiRecognizer, SetLogLevel
import subprocess
import json
import os
import argparse

SetLogLevel(0)


class Recognizer(object):

    def __init__(self, model):
        if not os.path.exists(model):
            print(
                "Please download the model from https://alphacephei.com/vosk/models and unpack as 'model' in the "
                "current folder.")
            exit(1)
        self.model = Model(str(model))

    def recognize(self, wav):

        """
        Распознавание речи

        Аргументы:
            wav: наименование аудио файла
        Результат:
            anonymized_text: распознанный текст
        """
        sample_rate = 16000

        print(wav)

        process = subprocess.Popen(['ffmpeg', '-loglevel', 'quiet', '-i',
                                    wav,
                                    '-ar', str(sample_rate), '-ac', '1', '-f', 's16le', '-'],
                                   stdout=subprocess.PIPE)

        rec = KaldiRecognizer(self.model, sample_rate)
        rec.SetWords(True)

        results, textResults = [], []

        while True:
            data = process.stdout.read(4000)

            if len(data) == 0:
                break

            if rec.AcceptWaveform(data):
                resultDict = json.loads(rec.Result())
                results.append(resultDict)
                textResults.append(resultDict.get("text", ""))
            else:
                print(rec.PartialResult())

        # process "final" result
        resultDict = json.loads(rec.FinalResult())
        results.append(resultDict)
        textResults.append(resultDict.get("text", ""))

        anonymized_text = ' '.join(textResults)

        return anonymized_text, results


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Утилита для распознавания речи')
    parser.add_argument('-m', '--model', metavar='MDL', help='Путь к директории модели распознавания')
    parser.add_argument('wav', metavar='WAV', help='Путь к .WAV файлу аудио')

    args = parser.parse_args()

    wav = args.wav
    model = Path(args.model)
    print(model)
    print(wav)

    recognizer = Recognizer(model)
    text, text_time = recognizer.recognize(wav)
    print(text)
